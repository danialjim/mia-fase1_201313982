#include <stdio.h>
#include <stdlib.h>
#include <sys/stat.h>
#include <string.h>
#include <time.h>
#define TRUE 1


typedef struct{
    char part_status[100];
    char part_type[100];
    char part_fit[100];
    int part_start;
    int part_size;
    char part_name[25];
}partition;

typedef struct{
    int mbr_tamanio;
    char mbr_fecha_creacion[128];
    int mbr_disk_signature;
    partition *mbr_partition1;
    partition *mbr_partition2;
    partition *mbr_partition3;
    partition *mbr_partition4;
}mbr;

typedef struct{
    char path[100];
    char id[10];
    char name[100];
    struct registro *siguiente;
    struct registro *anterior;
}registro;

typedef struct{
    registro *primero;
    registro *ultimo;
}tabla;


tabla *tablaDeParticiones;
void main(int argc, char *argv[])
{
    char cadenas[100];
    char cadenas2[100];
    char cadenas3[100];
    tablaDeParticiones = (tabla *)malloc(sizeof(tabla));
    tablaDeParticiones->primero = (registro *)malloc(sizeof(registro));
    tablaDeParticiones->ultimo = (registro *)malloc(sizeof(registro));
    tablaDeParticiones->primero = NULL;
    tablaDeParticiones->ultimo = NULL;

    printf("Ingresa un comando:\n");



    while(TRUE == 1){
    gets(cadenas2);


    while(strstr(cadenas2,"#")){
        gets(cadenas2);
    }

    while(strstr(cadenas2,"\\")){
        char delimitador[2] = "\t\\";
        char *token;
        token = strtok(cadenas2,delimitador);
        strcat(cadenas3,token);
        gets(cadenas2);
    }

    int jaja = 0;
    while(strstr(cadenas2,"\t") && jaja==0){
        char delimitador[2] = "\t";
        char *token;
        token = strtok(cadenas2,delimitador);
        strcat(cadenas3,token);
        jaja = 1;
    }

    if(strcmp(cadenas3,"") == 0)
        strcpy(cadenas,cadenas2);
    else
    strcpy(cadenas,cadenas3);




    if(strcmp(cadenas,"exit") == 0)
        break;
    /*PASO TODO A MINUSCULAS*/
    char *s;
    for(s = cadenas; *s; s++)
    *s = tolower((unsigned char)*s);
    puts(cadenas);

    /*SEPARO LAS CADENAS*/
    char delimitador[2] = " ";
    char *token;


    token = strtok(cadenas,delimitador);

    char mkdisk[10] = "mkdisk";
    char rmdisk[10] = "rmdisk";
    char fdisk[10] = "fdisk";
    char mount[10] = "mount";
    char unmount[10] = "unmount";
    char rep[10] = "rep";
    if(strcmp(mkdisk,token) == 0)
        crearDisco(token);
    if(strcmp(rmdisk,token) == 0)
        eliminarDisco(token);
    if(strcmp(fdisk,token) == 0)
        administrarParticiones(token);
    if(strcmp(mount,token) == 0)
        metodoMount(token);
    if(strcmp(unmount,token) == 0)
        metodoUnmount(token);
    if(strcmp(rep,token) == 0)
        metodoRep(token);
}
}

int crearDisco(char *token)
{
    char size[100] = "-size";
    char unit[100] = "-unit";
    char path[200] = "-path";
    char comillas[2] = "\"";
    int control;

    int TamanioDeDisco;
    char unitLetra[5] = "m";
    char direccionPath[200];
    char direccionPath2[200];
    char direccionPath3[200];


    while( token != NULL )
       {
          token = strtok(NULL, " =");
          if(token==NULL)
              break;


          char *estaSize;
          char *estaUnit;
          char *estaPath;

          estaSize = strstr(token,size);
          estaUnit = strstr(token,unit);
          estaPath = strstr(token,path);

          if(estaSize){
              control = 1;
          }else if(estaUnit){
              control = 2;
          }else if(estaPath){
              control = 3;
          }else{
              control = 0;
          }

          switch (control) {
          case 1:
              token = strtok(NULL, " =");
              TamanioDeDisco = atoi(token);

              if(TamanioDeDisco > 0){
              }
              else{
                  printf("\nTAMANIO NO VALIDO");
                  return -1;
              }
              break;
          case 2:
              token = strtok(NULL, " =");
              strcpy(unitLetra,token);
              if((strcmp(unitLetra,"m") == 0) || (strcmp(unitLetra,"k") == 0)){

              }
              else{
                  printf("\nERROR, UNIDAD NO VALIDA");
                  return -1;
              }

              break;
          case 3:

              token = strtok(NULL, " =");

              if(strstr(token,comillas)){
                  strcpy(direccionPath,token+1);
                  token = strtok(NULL, "\"");
                  strcat(direccionPath," ");
                  strcat(direccionPath,token);
                  strcpy(direccionPath2,direccionPath);
                  strcpy(direccionPath3,direccionPath);
              }else{
                  strcpy(direccionPath,token);
                  strcpy(direccionPath2,token);
                  strcpy(direccionPath3,token);
              }
              break;
          default:
              printf("Error, parametro no valido\n");
              return -1;
              break;
          }
}

    /*CREAR EL PATH*/
    int numPalabras = 1;
    char *tem;
    tem = strtok(direccionPath,"/");
    while(tem!=NULL)
    {
        tem = strtok(NULL,"/");
        if(tem==NULL)
            break;
        numPalabras++;
    }

    char pathReal[200] = "/";
    char *temm;
    temm = strtok(direccionPath2,"/");
    strcat(pathReal,temm);
    strcat(pathReal,"/");
    while(numPalabras > 2)
    {
        temm = strtok(NULL,"/");
        strcat(pathReal,temm);
        strcat(pathReal,"/");
        mkdir(pathReal ,S_IRWXU | S_IRWXG | S_IROTH | S_IXOTH);
        numPalabras--;
    }

    temm = strtok(NULL,"/");

    mbr *mbr1 = (mbr *)malloc(sizeof(mbr));
    FILE *archivo = fopen(direccionPath3,"wb");
    time_t tiempo = time(0);
            struct tm *tlocal = localtime(&tiempo);
            char output[128];
            strftime(output,128,"%d/%m/%y %H:%M:%S",tlocal);
    strcpy(mbr1->mbr_fecha_creacion,output);
    int random = rand();
    mbr1->mbr_disk_signature = random;
    mbr1->mbr_partition1 = (partition *)malloc(sizeof(partition));
    mbr1->mbr_partition2 = (partition *)malloc(sizeof(partition));
    mbr1->mbr_partition3 = (partition *)malloc(sizeof(partition));
    mbr1->mbr_partition4 = (partition *)malloc(sizeof(partition));
    strcpy(mbr1->mbr_partition1->part_type,"x");
    strcpy(mbr1->mbr_partition2->part_type,"x");
    strcpy(mbr1->mbr_partition3->part_type,"x");
    strcpy(mbr1->mbr_partition4->part_type,"x");
    mbr1->mbr_partition1->part_size = 0;
    mbr1->mbr_partition2->part_size = 0;
    mbr1->mbr_partition3->part_size = 0;
    mbr1->mbr_partition4->part_size = 0;

    if(strcmp(unitLetra,"k") == 0)
    {
        mbr1->mbr_tamanio = TamanioDeDisco*1024;
        fwrite(mbr1,sizeof(mbr),1,archivo);
        for(int i = 0; i < (500*TamanioDeDisco); i++)
            fputs("\\0",archivo);

    }else if(strcmp(unitLetra,"m") == 0)
    {
        mbr1->mbr_tamanio = TamanioDeDisco*1024*1024;
        fwrite(mbr1,sizeof(mbr),1,archivo);
        for(int i = 0; i < 2*(500*TamanioDeDisco*500); i++)
            fputs("\\0",archivo);
    }

    fclose(archivo);

    mbr *mbrLeido = (mbr *)malloc(sizeof(mbr));
    FILE *barchivo1 = fopen(direccionPath3,"rb");
    fread(mbrLeido,sizeof(mbr),1,barchivo1);
    int t = sizeof(mbrLeido);
    fclose(barchivo1);


    return 1;
}


int eliminarDisco(char *token)
{
    char path[10] = "-path";
    char tomandoPath[200];
    char comillas[3] = "\"";

    char desicion[5];

    int k = 0;
    token = strtok(NULL, " =\"");
    while(token != NULL)
    {
        char *estaPath;
        estaPath = strstr(token,path);
        if(estaPath){
            token = strtok(NULL, " =");

            if(strstr(token,comillas)){
                strcpy(tomandoPath,token+1);
                token = strtok(NULL, "\"");
                strcat(tomandoPath," ");
                strcat(tomandoPath,token);
                printf("\nCAAAAAAAAAADENA   : %s\n",tomandoPath);
            }else{
                strcpy(tomandoPath,token);
            }
        }
        token = strtok(NULL, " =\"");
        if(token==NULL){
             break;
        }

        else{
            k = 1;
            printf("ERROR, Comando no valido.\n");
            break;
        }
    }

    if(k == 0){
            printf( "%s\n", tomandoPath );

                printf("si: Eliminar disco \nno: No elimnar el disco\n");
                gets(desicion);
                if(strcmp(desicion,"si") == 0)
                {
                    FILE *archivo = fopen(tomandoPath,"r");

                    if(archivo!= NULL){
                        remove(tomandoPath);
                        fclose(archivo);
                        printf("\nDISCO ELIMINADO");
                        return 1;
                    }else{
                        printf("DISCO NO EXISTENTE\n");
                    }

                }else{
                    printf("Disco no eliminado");
                }

       }else{
             printf("Error, comando no valido");
       }

}

int administrarParticiones(char *token)
{
    char size[100] = "-size";
    char unit[100] = "-unit";
    char path[200] = "-path";
    char type[10] = "-type";
    char fit[10] = "-fit";
    char deletee[200] = "-delete";
    char name[10] = "-name";
    char add[10] = "-add";
    char comillas[3] = "\"";

    int tamanioParticion = 0;
    char letraUnit[100] = "k";
    char direccion[200];
    char letraType[100] = "p";
    char letraFit[100] = "wf";
    char letraDelete[100];
    char letraName[100];
    int tamanioAdd = 0;

    int control;
    int accion = 0;
    while(token != NULL)
    {
        token = strtok(NULL, " =");
        if(token==NULL)
            break;

        if(strstr(token,size))
            control = 1;
        else if(strstr(token,unit))
            control = 2;
        else if(strstr(token,path))
            control = 3;
        else if(strstr(token,type))
            control = 4;
        else if(strstr(token,fit))
            control = 5;
        else if(strstr(token,deletee))
            control = 6;
        else if(strstr(token,name))
            control = 7;
        else if(strstr(token,add))
            control = 8;
        else
            control = 0;

        switch (control) {
        case 1:
            token = strtok(NULL, " =");
            tamanioParticion = atoi(token);
            if(tamanioParticion <= 0)
            {
                printf("\nError, Tamanio de la particion invalido\n");
                return -1;
            }
            break;
        case 2:
            token = strtok(NULL, " =");
            strcpy(letraUnit,token);
            if(strcmp(letraUnit,"b") == 0 || (strcmp(letraUnit,"m") == 0) || (strcmp(letraUnit,"k") == 0))
                printf("\nVALIDO DE UNIT: ");
            else{
                printf("\nERROR, UNIDAD NO VALIDA");
                return -1;
            }
            break;
        case 3:
            token = strtok(NULL, " =");

            if(strstr(token,comillas)){
                strcpy(direccion,token+1);
                token = strtok(NULL, "\"");
                strcat(direccion," ");
                strcat(direccion,token);
            }else{
                strcpy(direccion,token);
            }

            /*COMPROBANDO SI EXISTE EL DISCO*/
            FILE *archivoDisco = fopen(direccion,"rb");
            if(archivoDisco==NULL){
                printf("\nERROR: DISCO NO EXISTE.\n");
                return -1;
            }else{
                close(archivoDisco);
            }
            break;
        case 4:
            token = strtok(NULL, " =");
            strcpy(letraType,token);
            if(strcmp(letraType,"p") == 0 || (strcmp(letraType,"e") == 0) || (strcmp(letraType,"l") == 0))
                printf("\nVALIDO DE TYPE: ");
            else{
                printf("\nERROR, UNIDAD NO VALIDA");
                return -1;
            }
            break;
        case 5:
            token = strtok(NULL, " =");
            strcpy(letraFit,token);
            if(strcmp(letraFit,"bf") == 0 || (strcmp(letraFit,"ff") == 0) || (strcmp(letraFit,"wf") == 0))
                printf("\nVALIDO DE FIT: ");
            else{
                printf("\nERROR, UNIDAD NO VALIDA");
                return -1;
            }
            break;
        case 6:
            token = strtok(NULL, " =");
            strcpy(letraDelete,token);
            if(strcmp(letraDelete,"fast") == 0 || (strcmp(letraDelete,"full") == 0)){
                printf("\nVALIDO DE DELETE: ");
                accion = 1;
            }
            else{
                printf("\nERROR, DELETE NO VALIDA");
                return -1;
            }
            break;
        case 7:
            token = strtok(NULL, " =");
            strcpy(letraName,token);
            break;
        case 8:
            token = strtok(NULL, " =");
            tamanioAdd = atoi(token);
            accion = 2;
            break;
        default:
            printf("Error, parametro no valido\n");
            return -1;
            break;
        }
    }


    FILE *binario = fopen(direccion,"r+b");
    mbr *compro = (mbr *)malloc(sizeof(mbr));
    fseek(binario,0,SEEK_SET);
    fread(compro,sizeof(mbr),1,binario);
    fclose(binario);

    int tama = 0;
    if(strcmp(letraUnit,"b") == 0)
    {
        tama = tamanioParticion;
    }else if(strcmp(letraUnit,"k") == 0){
        tama = tamanioParticion * 1024;
    }else if(strcmp(letraUnit,"m") == 0){
        tama = tamanioParticion * 1024 * 1024;
    }

    int tamaAdd = 0;
    if(strcmp(letraUnit,"b") == 0)
    {
        tamaAdd = tamanioAdd;
    }else if(strcmp(letraUnit,"k") == 0){
        tamaAdd = tamanioAdd * 1024;
    }else if(strcmp(letraUnit,"m") == 0){
        tamaAdd = tamanioAdd * 1024 * 1024;
    }

    int espacioLibreEnDisco = compro->mbr_tamanio - compro->mbr_partition1->part_size - compro->mbr_partition2->part_size - compro->mbr_partition3->part_size - compro->mbr_partition4->part_size ;


    int extendidas = 1;
    int primarias = 0;
    int acceso = 0;
    if(accion == 0){
        if(strcmp(letraType,"e") == 0){
            if(strcmp(compro->mbr_partition1->part_type,"e") == 0 || strcmp(compro->mbr_partition2->part_type,"e") == 0 || strcmp(compro->mbr_partition3->part_type,"e") == 0 || strcmp(compro->mbr_partition4->part_type,"e") == 0){
                extendidas = 0;
                printf("\nError, Ya existe una particion extendida\n");
                return -1;
            }else{
                if(espacioLibreEnDisco > 0 && espacioLibreEnDisco >= tama){
                    if(strcmp(compro->mbr_partition1->part_type,"x") == 0){
                        if(strcmp(compro->mbr_partition2->part_type,"x") == 0){
                            if(strcmp(compro->mbr_partition3->part_type,"x") == 0){
                                if(strcmp(compro->mbr_partition4->part_type,"x") == 0){
                                    mbr *mbrLeido = (mbr *)malloc(sizeof(mbr));
                                    mbr *mbrLeido1 = (mbr *)malloc(sizeof(mbr));
                                    mbrLeido1->mbr_partition1 = (partition *)malloc(sizeof(partition));
                                    FILE *barchivo1 = fopen(direccion,"r+b");
                                    fseek(barchivo1,0,SEEK_SET);
                                    fread(mbrLeido,sizeof(mbr),1,barchivo1);
                                    mbrLeido->mbr_partition1 = (partition *)malloc(sizeof(partition));
                                    mbrLeido->mbr_partition1->part_size = tama;
                                    mbrLeido->mbr_partition1->part_start = sizeof(mbrLeido);
                                    strcpy(mbrLeido->mbr_partition1->part_fit,letraFit);
                                    strcpy(mbrLeido->mbr_partition1->part_name,letraName);
                                    strcpy(mbrLeido->mbr_partition1->part_type,letraType);
                                    strcpy(mbrLeido->mbr_partition1->part_status,"1");

                                    fseek(barchivo1,0,SEEK_SET);
                                    fwrite(mbrLeido,sizeof(mbr)+ sizeof( partition),1,barchivo1);
                                    fseek(barchivo1,0,SEEK_SET);
                                    fread(mbrLeido1,sizeof(mbr),1,barchivo1);
                                    fclose(barchivo1);
                                    acceso = 1;
                                }else{
                                    if(tama <= compro->mbr_partition4->part_start){
                                        mbr *mbrLeido = (mbr *)malloc(sizeof(mbr));
                                        mbr *mbrLeido1 = (mbr *)malloc(sizeof(mbr));
                                        mbrLeido1->mbr_partition1 = (partition *)malloc(sizeof(partition));
                                        FILE *barchivo1 = fopen(direccion,"r+b");
                                        fseek(barchivo1,0,SEEK_SET);
                                        fread(mbrLeido,sizeof(mbr),1,barchivo1);
                                        mbrLeido->mbr_partition1 = (partition *)malloc(sizeof(partition));
                                        mbrLeido->mbr_partition1->part_size = tama;
                                        mbrLeido->mbr_partition1->part_start = sizeof(mbrLeido);
                                        strcpy(mbrLeido->mbr_partition1->part_fit,letraFit);
                                        strcpy(mbrLeido->mbr_partition1->part_name,letraName);
                                        strcpy(mbrLeido->mbr_partition1->part_type,letraType);
                                        strcpy(mbrLeido->mbr_partition1->part_status,"1");

                                        fseek(barchivo1,0,SEEK_SET);
                                        fwrite(mbrLeido,sizeof(mbr)+ sizeof( partition),1,barchivo1);
                                        fseek(barchivo1,0,SEEK_SET);
                                        fread(mbrLeido1,sizeof(mbr),1,barchivo1);
                                        fclose(barchivo1);
                                        acceso = 1;
                                    }
                                }
                            }else{
                                if(tama <= compro->mbr_partition3->part_start){
                                    mbr *mbrLeido = (mbr *)malloc(sizeof(mbr));
                                    mbr *mbrLeido1 = (mbr *)malloc(sizeof(mbr));
                                    mbrLeido1->mbr_partition1 = (partition *)malloc(sizeof(partition));
                                    FILE *barchivo1 = fopen(direccion,"r+b");
                                    fseek(barchivo1,0,SEEK_SET);
                                    fread(mbrLeido,sizeof(mbr),1,barchivo1);
                                    mbrLeido->mbr_partition1 = (partition *)malloc(sizeof(partition));
                                    mbrLeido->mbr_partition1->part_size = tama;
                                    mbrLeido->mbr_partition1->part_start = sizeof(mbrLeido);
                                    strcpy(mbrLeido->mbr_partition1->part_fit,letraFit);
                                    strcpy(mbrLeido->mbr_partition1->part_name,letraName);
                                    strcpy(mbrLeido->mbr_partition1->part_type,letraType);
                                    strcpy(mbrLeido->mbr_partition1->part_status,"1");

                                    fseek(barchivo1,0,SEEK_SET);
                                    fwrite(mbrLeido,sizeof(mbr)+ sizeof( partition),1,barchivo1);
                                    fseek(barchivo1,0,SEEK_SET);
                                    fread(mbrLeido1,sizeof(mbr),1,barchivo1);
                                    fclose(barchivo1);
                                    acceso = 1;
                                }
                            }
                        }else{
                            if(tama <= compro->mbr_partition2->part_start){
                                mbr *mbrLeido = (mbr *)malloc(sizeof(mbr));
                                mbr *mbrLeido1 = (mbr *)malloc(sizeof(mbr));
                                mbrLeido1->mbr_partition1 = (partition *)malloc(sizeof(partition));
                                FILE *barchivo1 = fopen(direccion,"r+b");
                                fseek(barchivo1,0,SEEK_SET);
                                fread(mbrLeido,sizeof(mbr),1,barchivo1);
                                mbrLeido->mbr_partition1 = (partition *)malloc(sizeof(partition));
                                mbrLeido->mbr_partition1->part_size = tama;
                                mbrLeido->mbr_partition1->part_start = sizeof(mbrLeido);
                                strcpy(mbrLeido->mbr_partition1->part_fit,letraFit);
                                strcpy(mbrLeido->mbr_partition1->part_name,letraName);
                                strcpy(mbrLeido->mbr_partition1->part_type,letraType);
                                strcpy(mbrLeido->mbr_partition1->part_status,"1");

                                fseek(barchivo1,0,SEEK_SET);
                                fwrite(mbrLeido,sizeof(mbr)+ sizeof( partition),1,barchivo1);
                                fseek(barchivo1,0,SEEK_SET);
                                fread(mbrLeido1,sizeof(mbr),1,barchivo1);
                                fclose(barchivo1);
                                acceso = 1;
                            }
                        }
                    }

                    if(strcmp(compro->mbr_partition2->part_type,"x") == 0 && acceso == 0){
                        if(strcmp(compro->mbr_partition3->part_type,"x") == 0){
                            if(strcmp(compro->mbr_partition4->part_type,"x") == 0){
                                mbr *mbrLeido = (mbr *)malloc(sizeof(mbr));
                                mbr *mbrLeido1 = (mbr *)malloc(sizeof(mbr));
                                mbrLeido1->mbr_partition2 = (partition *)malloc(sizeof(partition));
                                FILE *barchivo1 = fopen(direccion,"r+b");
                                fseek(barchivo1,0,SEEK_SET);
                                fread(mbrLeido,sizeof(mbr),1,barchivo1);
                                mbrLeido->mbr_partition2 = (partition *)malloc(sizeof(partition));
                                mbrLeido->mbr_partition2->part_size = tama;
                                mbrLeido->mbr_partition2->part_start = sizeof(mbrLeido) + mbrLeido->mbr_partition1->part_size;
                                strcpy(mbrLeido->mbr_partition2->part_fit,letraFit);
                                strcpy(mbrLeido->mbr_partition2->part_name,letraName);
                                strcpy(mbrLeido->mbr_partition2->part_type,letraType);
                                strcpy(mbrLeido->mbr_partition2->part_status,"1");

                                fseek(barchivo1,0,SEEK_SET);
                                fwrite(mbrLeido,sizeof(mbr)+ sizeof( partition),1,barchivo1);
                                fseek(barchivo1,0,SEEK_SET);
                                fread(mbrLeido1,sizeof(mbr),1,barchivo1);
                                fclose(barchivo1);
                                acceso = 1;
                            }else{
                                if(tama <= (compro->mbr_partition4->part_start - (compro->mbr_partition1->part_size + sizeof(mbr)))){
                                    mbr *mbrLeido = (mbr *)malloc(sizeof(mbr));
                                    mbr *mbrLeido1 = (mbr *)malloc(sizeof(mbr));
                                    mbrLeido1->mbr_partition2 = (partition *)malloc(sizeof(partition));
                                    FILE *barchivo1 = fopen(direccion,"r+b");
                                    fseek(barchivo1,0,SEEK_SET);
                                    fread(mbrLeido,sizeof(mbr),1,barchivo1);
                                    mbrLeido->mbr_partition2 = (partition *)malloc(sizeof(partition));
                                    mbrLeido->mbr_partition2->part_size = tama;
                                    mbrLeido->mbr_partition2->part_start = sizeof(mbrLeido) + mbrLeido->mbr_partition1->part_size;
                                    strcpy(mbrLeido->mbr_partition2->part_fit,letraFit);
                                    strcpy(mbrLeido->mbr_partition2->part_name,letraName);
                                    strcpy(mbrLeido->mbr_partition2->part_type,letraType);
                                    strcpy(mbrLeido->mbr_partition2->part_status,"1");

                                    fseek(barchivo1,0,SEEK_SET);
                                    fwrite(mbrLeido,sizeof(mbr)+ sizeof( partition),1,barchivo1);
                                    fseek(barchivo1,0,SEEK_SET);
                                    fread(mbrLeido1,sizeof(mbr),1,barchivo1);
                                    fclose(barchivo1);
                                    acceso = 1;
                                }
                            }
                        }else{
                            if(tama <= (compro->mbr_partition3->part_start - (compro->mbr_partition1->part_size + sizeof(mbr)))){
                                mbr *mbrLeido = (mbr *)malloc(sizeof(mbr));
                                mbr *mbrLeido1 = (mbr *)malloc(sizeof(mbr));
                                mbrLeido1->mbr_partition2 = (partition *)malloc(sizeof(partition));
                                FILE *barchivo1 = fopen(direccion,"r+b");
                                fseek(barchivo1,0,SEEK_SET);
                                fread(mbrLeido,sizeof(mbr),1,barchivo1);
                                mbrLeido->mbr_partition2 = (partition *)malloc(sizeof(partition));
                                mbrLeido->mbr_partition2->part_size = tama;
                                mbrLeido->mbr_partition2->part_start = sizeof(mbrLeido) + mbrLeido->mbr_partition1->part_size;
                                strcpy(mbrLeido->mbr_partition2->part_fit,letraFit);
                                strcpy(mbrLeido->mbr_partition2->part_name,letraName);
                                strcpy(mbrLeido->mbr_partition2->part_type,letraType);
                                strcpy(mbrLeido->mbr_partition2->part_status,"1");

                                fseek(barchivo1,0,SEEK_SET);
                                fwrite(mbrLeido,sizeof(mbr)+ sizeof( partition),1,barchivo1);
                                fseek(barchivo1,0,SEEK_SET);
                                fread(mbrLeido1,sizeof(mbr),1,barchivo1);
                                fclose(barchivo1);
                                acceso = 1;
                            }
                        }
                    }
                    if(strcmp(compro->mbr_partition3->part_type,"x") == 0 && acceso == 0){
                        if(strcmp(compro->mbr_partition4->part_type,"x") == 0){
                            mbr *mbrLeido = (mbr *)malloc(sizeof(mbr));
                            mbr *mbrLeido1 = (mbr *)malloc(sizeof(mbr));
                            mbrLeido1->mbr_partition3 = (partition *)malloc(sizeof(partition));
                            FILE *barchivo1 = fopen(direccion,"r+b");
                            fseek(barchivo1,0,SEEK_SET);
                            fread(mbrLeido,sizeof(mbr),1,barchivo1);
                            mbrLeido->mbr_partition3 = (partition *)malloc(sizeof(partition));
                            mbrLeido->mbr_partition3->part_size = tama;
                            mbrLeido->mbr_partition3->part_start = sizeof(mbrLeido) + (mbrLeido->mbr_partition2->part_start + mbrLeido->mbr_partition2->part_size);
                            strcpy(mbrLeido->mbr_partition3->part_fit,letraFit);
                            strcpy(mbrLeido->mbr_partition3->part_name,letraName);
                            strcpy(mbrLeido->mbr_partition3->part_type,letraType);
                            strcpy(mbrLeido->mbr_partition3->part_status,"1");

                            fseek(barchivo1,0,SEEK_SET);
                            fwrite(mbrLeido,sizeof(mbr)+ sizeof( partition),1,barchivo1);
                            fseek(barchivo1,0,SEEK_SET);
                            fread(mbrLeido1,sizeof(mbr),1,barchivo1);
                            fclose(barchivo1);
                            acceso = 1;
                        }else{
                            if(tama <= (compro->mbr_partition4->part_start - (compro->mbr_partition2->part_start + compro->mbr_partition2->part_size))){
                                mbr *mbrLeido = (mbr *)malloc(sizeof(mbr));
                                mbr *mbrLeido1 = (mbr *)malloc(sizeof(mbr));
                                mbrLeido1->mbr_partition3 = (partition *)malloc(sizeof(partition));
                                FILE *barchivo1 = fopen(direccion,"r+b");
                                fseek(barchivo1,0,SEEK_SET);
                                fread(mbrLeido,sizeof(mbr),1,barchivo1);
                                mbrLeido->mbr_partition3 = (partition *)malloc(sizeof(partition));
                                mbrLeido->mbr_partition3->part_size = tama;
                                mbrLeido->mbr_partition3->part_start = sizeof(mbrLeido) + (mbrLeido->mbr_partition2->part_start + mbrLeido->mbr_partition2->part_size);
                                strcpy(mbrLeido->mbr_partition3->part_fit,letraFit);
                                strcpy(mbrLeido->mbr_partition3->part_name,letraName);
                                strcpy(mbrLeido->mbr_partition3->part_type,letraType);
                                strcpy(mbrLeido->mbr_partition3->part_status,"1");

                                fseek(barchivo1,0,SEEK_SET);
                                fwrite(mbrLeido,sizeof(mbr)+ sizeof( partition),1,barchivo1);
                                fseek(barchivo1,0,SEEK_SET);
                                fread(mbrLeido1,sizeof(mbr),1,barchivo1);
                                fclose(barchivo1);
                                acceso = 1;
                            }
                        }
                    }
                    if(strcmp(compro->mbr_partition4->part_type,"x") == 0 && acceso == 0){
                        mbr *mbrLeido = (mbr *)malloc(sizeof(mbr));
                        mbr *mbrLeido1 = (mbr *)malloc(sizeof(mbr));
                        mbrLeido1->mbr_partition4 = (partition *)malloc(sizeof(partition));
                        FILE *barchivo1 = fopen(direccion,"r+b");
                        fseek(barchivo1,0,SEEK_SET);
                        fread(mbrLeido,sizeof(mbr),1,barchivo1);
                        mbrLeido->mbr_partition4 = (partition *)malloc(sizeof(partition));
                        mbrLeido->mbr_partition4->part_size = tama;
                        mbrLeido->mbr_partition4->part_start = sizeof(mbrLeido) + mbrLeido->mbr_partition3->part_start + mbrLeido->mbr_partition3->part_size;
                        strcpy(mbrLeido->mbr_partition4->part_fit,letraFit);
                        strcpy(mbrLeido->mbr_partition4->part_name,letraName);
                        strcpy(mbrLeido->mbr_partition4->part_type,letraType);
                        strcpy(mbrLeido->mbr_partition4->part_status,"1");

                        fseek(barchivo1,0,SEEK_SET);
                        fwrite(mbrLeido,sizeof(mbr)+ sizeof( partition),1,barchivo1);
                        fseek(barchivo1,0,SEEK_SET);
                        fread(mbrLeido1,sizeof(mbr),1,barchivo1);
                        fclose(barchivo1);
                        acceso = 1;
                    }
                }else{
                    printf("\nERROR, NO HAY ESPACIO\n");
                }
            }
        }else if(strcmp(letraType,"p") == 0){
            if(strcmp(compro->mbr_partition1->part_type,"p") == 0 )
                primarias++;
            if(strcmp(compro->mbr_partition2->part_type,"p") == 0 )
                primarias++;
            if(strcmp(compro->mbr_partition3->part_type,"p") == 0 )
                primarias++;
            if(strcmp(compro->mbr_partition4->part_type,"p") == 0 )
                primarias++;

            if(primarias < 3){
                if(espacioLibreEnDisco > 0 && espacioLibreEnDisco >= tama){
                    if(strcmp(compro->mbr_partition1->part_type,"x") == 0){
                        if(strcmp(compro->mbr_partition2->part_type,"x") == 0){
                            if(strcmp(compro->mbr_partition3->part_type,"x") == 0){
                                if(strcmp(compro->mbr_partition4->part_type,"x") == 0){
                                    mbr *mbrLeido = (mbr *)malloc(sizeof(mbr));
                                    mbr *mbrLeido1 = (mbr *)malloc(sizeof(mbr));
                                    mbrLeido1->mbr_partition1 = (partition *)malloc(sizeof(partition));
                                    FILE *barchivo1 = fopen(direccion,"r+b");
                                    fseek(barchivo1,0,SEEK_SET);
                                    fread(mbrLeido,sizeof(mbr),1,barchivo1);
                                    mbrLeido->mbr_partition1 = (partition *)malloc(sizeof(partition));
                                    mbrLeido->mbr_partition1->part_size = tama;
                                    mbrLeido->mbr_partition1->part_start = sizeof(mbrLeido);
                                    strcpy(mbrLeido->mbr_partition1->part_fit,letraFit);
                                    strcpy(mbrLeido->mbr_partition1->part_name,letraName);
                                    strcpy(mbrLeido->mbr_partition1->part_type,letraType);
                                    strcpy(mbrLeido->mbr_partition1->part_status,"1");

                                    fseek(barchivo1,0,SEEK_SET);
                                    fwrite(mbrLeido,sizeof(mbr)+ sizeof( partition),1,barchivo1);
                                    fseek(barchivo1,0,SEEK_SET);
                                    fread(mbrLeido1,sizeof(mbr),1,barchivo1);
                                    fclose(barchivo1);
                                    acceso = 1;
                                }else{
                                    if(tama <= compro->mbr_partition4->part_start){
                                        mbr *mbrLeido = (mbr *)malloc(sizeof(mbr));
                                        mbr *mbrLeido1 = (mbr *)malloc(sizeof(mbr));
                                        mbrLeido1->mbr_partition1 = (partition *)malloc(sizeof(partition));
                                        FILE *barchivo1 = fopen(direccion,"r+b");
                                        fseek(barchivo1,0,SEEK_SET);
                                        fread(mbrLeido,sizeof(mbr),1,barchivo1);
                                        mbrLeido->mbr_partition1 = (partition *)malloc(sizeof(partition));
                                        mbrLeido->mbr_partition1->part_size = tama;
                                        mbrLeido->mbr_partition1->part_start = sizeof(mbrLeido);
                                        strcpy(mbrLeido->mbr_partition1->part_fit,letraFit);
                                        strcpy(mbrLeido->mbr_partition1->part_name,letraName);
                                        strcpy(mbrLeido->mbr_partition1->part_type,letraType);
                                        strcpy(mbrLeido->mbr_partition1->part_status,"1");

                                        fseek(barchivo1,0,SEEK_SET);
                                        fwrite(mbrLeido,sizeof(mbr)+ sizeof( partition),1,barchivo1);
                                        fseek(barchivo1,0,SEEK_SET);
                                        fread(mbrLeido1,sizeof(mbr),1,barchivo1);
                                        fclose(barchivo1);
                                        acceso = 1;
                                    }
                                }
                            }else{
                                if(tama <= compro->mbr_partition3->part_start){
                                    mbr *mbrLeido = (mbr *)malloc(sizeof(mbr));
                                    mbr *mbrLeido1 = (mbr *)malloc(sizeof(mbr));
                                    mbrLeido1->mbr_partition1 = (partition *)malloc(sizeof(partition));
                                    FILE *barchivo1 = fopen(direccion,"r+b");
                                    fseek(barchivo1,0,SEEK_SET);
                                    fread(mbrLeido,sizeof(mbr),1,barchivo1);
                                    mbrLeido->mbr_partition1 = (partition *)malloc(sizeof(partition));
                                    mbrLeido->mbr_partition1->part_size = tama;
                                    mbrLeido->mbr_partition1->part_start = sizeof(mbrLeido);
                                    strcpy(mbrLeido->mbr_partition1->part_fit,letraFit);
                                    strcpy(mbrLeido->mbr_partition1->part_name,letraName);
                                    strcpy(mbrLeido->mbr_partition1->part_type,letraType);
                                    strcpy(mbrLeido->mbr_partition1->part_status,"1");

                                    fseek(barchivo1,0,SEEK_SET);
                                    fwrite(mbrLeido,sizeof(mbr)+ sizeof( partition),1,barchivo1);
                                    fseek(barchivo1,0,SEEK_SET);
                                    fread(mbrLeido1,sizeof(mbr),1,barchivo1);
                                    fclose(barchivo1);
                                    acceso = 1;
                                }
                            }
                        }else{
                            if(tama <= compro->mbr_partition2->part_start){
                                mbr *mbrLeido = (mbr *)malloc(sizeof(mbr));
                                mbr *mbrLeido1 = (mbr *)malloc(sizeof(mbr));
                                mbrLeido1->mbr_partition1 = (partition *)malloc(sizeof(partition));
                                FILE *barchivo1 = fopen(direccion,"r+b");
                                fseek(barchivo1,0,SEEK_SET);
                                fread(mbrLeido,sizeof(mbr),1,barchivo1);
                                mbrLeido->mbr_partition1 = (partition *)malloc(sizeof(partition));
                                mbrLeido->mbr_partition1->part_size = tama;
                                mbrLeido->mbr_partition1->part_start = sizeof(mbrLeido);
                                strcpy(mbrLeido->mbr_partition1->part_fit,letraFit);
                                strcpy(mbrLeido->mbr_partition1->part_name,letraName);
                                strcpy(mbrLeido->mbr_partition1->part_type,letraType);
                                strcpy(mbrLeido->mbr_partition1->part_status,"1");

                                fseek(barchivo1,0,SEEK_SET);
                                fwrite(mbrLeido,sizeof(mbr)+ sizeof( partition),1,barchivo1);
                                fseek(barchivo1,0,SEEK_SET);
                                fread(mbrLeido1,sizeof(mbr),1,barchivo1);
                                fclose(barchivo1);
                                acceso = 1;
                            }
                        }
                    }
                    if(strcmp(compro->mbr_partition2->part_type,"x") == 0 && acceso == 0){
                        if(strcmp(compro->mbr_partition3->part_type,"x") == 0){
                            if(strcmp(compro->mbr_partition4->part_type,"x") == 0){
                                mbr *mbrLeido = (mbr *)malloc(sizeof(mbr));
                                mbr *mbrLeido1 = (mbr *)malloc(sizeof(mbr));
                                mbrLeido1->mbr_partition2 = (partition *)malloc(sizeof(partition));
                                FILE *barchivo1 = fopen(direccion,"r+b");
                                fseek(barchivo1,0,SEEK_SET);
                                fread(mbrLeido,sizeof(mbr),1,barchivo1);
                                mbrLeido->mbr_partition2 = (partition *)malloc(sizeof(partition));
                                mbrLeido->mbr_partition2->part_size = tama;
                                mbrLeido->mbr_partition2->part_start = sizeof(mbrLeido) + mbrLeido->mbr_partition1->part_size;
                                strcpy(mbrLeido->mbr_partition2->part_fit,letraFit);
                                strcpy(mbrLeido->mbr_partition2->part_name,letraName);
                                strcpy(mbrLeido->mbr_partition2->part_type,letraType);
                                strcpy(mbrLeido->mbr_partition2->part_status,"1");

                                fseek(barchivo1,0,SEEK_SET);
                                fwrite(mbrLeido,sizeof(mbr)+ sizeof( partition),1,barchivo1);
                                fseek(barchivo1,0,SEEK_SET);
                                fread(mbrLeido1,sizeof(mbr),1,barchivo1);
                                fclose(barchivo1);
                                acceso = 1;
                            }else{
                                if(tama <= (compro->mbr_partition4->part_start - (compro->mbr_partition1->part_size + compro->mbr_partition1->part_start))){
                                    mbr *mbrLeido = (mbr *)malloc(sizeof(mbr));
                                    mbr *mbrLeido1 = (mbr *)malloc(sizeof(mbr));
                                    mbrLeido1->mbr_partition2 = (partition *)malloc(sizeof(partition));
                                    FILE *barchivo1 = fopen(direccion,"r+b");
                                    fseek(barchivo1,0,SEEK_SET);
                                    fread(mbrLeido,sizeof(mbr),1,barchivo1);
                                    mbrLeido->mbr_partition2 = (partition *)malloc(sizeof(partition));
                                    mbrLeido->mbr_partition2->part_size = tama;
                                    mbrLeido->mbr_partition2->part_start = sizeof(mbrLeido) + mbrLeido->mbr_partition1->part_size;
                                    strcpy(mbrLeido->mbr_partition2->part_fit,letraFit);
                                    strcpy(mbrLeido->mbr_partition2->part_name,letraName);
                                    strcpy(mbrLeido->mbr_partition2->part_type,letraType);
                                    strcpy(mbrLeido->mbr_partition2->part_status,"1");

                                    fseek(barchivo1,0,SEEK_SET);
                                    fwrite(mbrLeido,sizeof(mbr)+ sizeof( partition),1,barchivo1);
                                    fseek(barchivo1,0,SEEK_SET);
                                    fread(mbrLeido1,sizeof(mbr),1,barchivo1);
                                    fclose(barchivo1);
                                    acceso = 1;
                                }
                            }
                        }else{
                            if(tama <= (compro->mbr_partition3->part_start - (compro->mbr_partition1->part_size +sizeof(mbr)))){
                                mbr *mbrLeido = (mbr *)malloc(sizeof(mbr));
                                mbr *mbrLeido1 = (mbr *)malloc(sizeof(mbr));
                                mbrLeido1->mbr_partition2 = (partition *)malloc(sizeof(partition));
                                FILE *barchivo1 = fopen(direccion,"r+b");
                                fseek(barchivo1,0,SEEK_SET);
                                fread(mbrLeido,sizeof(mbr),1,barchivo1);
                                mbrLeido->mbr_partition2 = (partition *)malloc(sizeof(partition));
                                mbrLeido->mbr_partition2->part_size = tama;
                                mbrLeido->mbr_partition2->part_start = sizeof(mbrLeido) + mbrLeido->mbr_partition1->part_size;
                                strcpy(mbrLeido->mbr_partition2->part_fit,letraFit);
                                strcpy(mbrLeido->mbr_partition2->part_name,letraName);
                                strcpy(mbrLeido->mbr_partition2->part_type,letraType);
                                strcpy(mbrLeido->mbr_partition2->part_status,"1");

                                fseek(barchivo1,0,SEEK_SET);
                                fwrite(mbrLeido,sizeof(mbr)+ sizeof( partition),1,barchivo1);
                                fseek(barchivo1,0,SEEK_SET);
                                fread(mbrLeido1,sizeof(mbr),1,barchivo1);
                                fclose(barchivo1);
                                acceso = 1;
                            }
                        }
                    }
                    if(strcmp(compro->mbr_partition3->part_type,"x") == 0 && acceso == 0){
                        if(strcmp(compro->mbr_partition4->part_type,"x") == 0){
                            mbr *mbrLeido = (mbr *)malloc(sizeof(mbr));
                            mbr *mbrLeido1 = (mbr *)malloc(sizeof(mbr));
                            mbrLeido1->mbr_partition3 = (partition *)malloc(sizeof(partition));
                            FILE *barchivo1 = fopen(direccion,"r+b");
                            fseek(barchivo1,0,SEEK_SET);
                            fread(mbrLeido,sizeof(mbr),1,barchivo1);
                            mbrLeido->mbr_partition3 = (partition *)malloc(sizeof(partition));
                            mbrLeido->mbr_partition3->part_size = tama;
                            mbrLeido->mbr_partition3->part_start = sizeof(mbrLeido) + (mbrLeido->mbr_partition2->part_start + mbrLeido->mbr_partition2->part_size);
                            strcpy(mbrLeido->mbr_partition3->part_fit,letraFit);
                            strcpy(mbrLeido->mbr_partition3->part_name,letraName);
                            strcpy(mbrLeido->mbr_partition3->part_type,letraType);
                            strcpy(mbrLeido->mbr_partition3->part_status,"1");

                            fseek(barchivo1,0,SEEK_SET);
                            fwrite(mbrLeido,sizeof(mbr)+ sizeof( partition),1,barchivo1);
                            fseek(barchivo1,0,SEEK_SET);
                            fread(mbrLeido1,sizeof(mbr),1,barchivo1);
                            fclose(barchivo1);
                            acceso = 1;
                        }else{
                            if(tama <= (compro->mbr_partition4->part_start - (compro->mbr_partition2->part_size + compro->mbr_partition2->part_start))){
                                mbr *mbrLeido = (mbr *)malloc(sizeof(mbr));
                                mbr *mbrLeido1 = (mbr *)malloc(sizeof(mbr));
                                mbrLeido1->mbr_partition3 = (partition *)malloc(sizeof(partition));
                                FILE *barchivo1 = fopen(direccion,"r+b");
                                fseek(barchivo1,0,SEEK_SET);
                                fread(mbrLeido,sizeof(mbr),1,barchivo1);
                                mbrLeido->mbr_partition3 = (partition *)malloc(sizeof(partition));
                                mbrLeido->mbr_partition3->part_size = tama;
                                mbrLeido->mbr_partition3->part_start = sizeof(mbrLeido) + (mbrLeido->mbr_partition2->part_start + mbrLeido->mbr_partition2->part_size);
                                strcpy(mbrLeido->mbr_partition3->part_fit,letraFit);
                                strcpy(mbrLeido->mbr_partition3->part_name,letraName);
                                strcpy(mbrLeido->mbr_partition3->part_type,letraType);
                                strcpy(mbrLeido->mbr_partition3->part_status,"1");

                                fseek(barchivo1,0,SEEK_SET);
                                fwrite(mbrLeido,sizeof(mbr)+ sizeof( partition),1,barchivo1);
                                fseek(barchivo1,0,SEEK_SET);
                                fread(mbrLeido1,sizeof(mbr),1,barchivo1);
                                fclose(barchivo1);
                                acceso = 1;
                            }
                        }
                    }
                    if(strcmp(compro->mbr_partition4->part_type,"x") == 0 && acceso == 0){
                        mbr *mbrLeido = (mbr *)malloc(sizeof(mbr));
                        mbr *mbrLeido1 = (mbr *)malloc(sizeof(mbr));
                        mbrLeido1->mbr_partition4 = (partition *)malloc(sizeof(partition));
                        FILE *barchivo1 = fopen(direccion,"r+b");
                        fseek(barchivo1,0,SEEK_SET);
                        fread(mbrLeido,sizeof(mbr),1,barchivo1);
                        mbrLeido->mbr_partition4 = (partition *)malloc(sizeof(partition));
                        mbrLeido->mbr_partition4->part_size = tama;
                        mbrLeido->mbr_partition4->part_start = sizeof(mbrLeido) + mbrLeido->mbr_partition3->part_start + mbrLeido->mbr_partition3->part_size;
                        strcpy(mbrLeido->mbr_partition4->part_fit,letraFit);
                        strcpy(mbrLeido->mbr_partition4->part_name,letraName);
                        strcpy(mbrLeido->mbr_partition4->part_type,letraType);
                        strcpy(mbrLeido->mbr_partition4->part_status,"1");

                        fseek(barchivo1,0,SEEK_SET);
                        fwrite(mbrLeido,sizeof(mbr)+ sizeof( partition),1,barchivo1);
                        fseek(barchivo1,0,SEEK_SET);
                        fread(mbrLeido1,sizeof(mbr),1,barchivo1);
                        fclose(barchivo1);
                        acceso = 1;
                    }
                }else{
                    printf("\nERROR, NO HAY ESPACIO\n");
                }
            }else{
                printf("\nERROR, Limite de primarias excedido\n");
            }

        }
    }
    if(accion == 1){
        FILE *binario = fopen(direccion,"r+b");
        mbr *compro = (mbr *)malloc(sizeof(mbr));
        fseek(binario,0,SEEK_SET);
        fread(compro,sizeof(mbr),1,binario);


        if(binario!=NULL){
            fclose(binario);
            if(strcmp(compro->mbr_partition1->part_name,letraName) == 0){
                if(strcmp(letraDelete,"full") == 0){
                    mbr *mbrLeido = (mbr *)malloc(sizeof(mbr));
                    mbr *mbrLeido1 = (mbr *)malloc(sizeof(mbr));
                    mbrLeido1->mbr_partition1 = (partition *)malloc(sizeof(partition));
                    FILE *barchivo1 = fopen(direccion,"r+b");
                    fseek(barchivo1,0,SEEK_SET);
                    fread(mbrLeido,sizeof(mbr),1,barchivo1);
                    mbrLeido->mbr_partition1 = (partition *)malloc(sizeof(partition));
                    mbrLeido->mbr_partition1->part_size = 0;
                    mbrLeido->mbr_partition1->part_start = 0;
                    strcpy(mbrLeido->mbr_partition1->part_fit,"");
                    strcpy(mbrLeido->mbr_partition1->part_name,"");
                    strcpy(mbrLeido->mbr_partition1->part_type,"x");

                    fseek(barchivo1,0,SEEK_SET);
                    fwrite(mbrLeido,sizeof(mbr)+ sizeof( partition),1,barchivo1);
                    fseek(barchivo1,0,SEEK_SET);
                    fread(mbrLeido1,sizeof(mbr),1,barchivo1);
                    fclose(barchivo1);
                }else if(strcmp(letraDelete,"fast") == 0){
                    mbr *mbrLeido = (mbr *)malloc(sizeof(mbr));
                    mbr *mbrLeido1 = (mbr *)malloc(sizeof(mbr));
                    mbrLeido1->mbr_partition1 = (partition *)malloc(sizeof(partition));
                    FILE *barchivo1 = fopen(direccion,"r+b");
                    fseek(barchivo1,0,SEEK_SET);
                    fread(mbrLeido,sizeof(mbr),1,barchivo1);
                    mbrLeido->mbr_partition1 = (partition *)malloc(sizeof(partition));
                    mbrLeido->mbr_partition1->part_size = 0;
                    mbrLeido->mbr_partition1->part_start = 0;
                    strcpy(mbrLeido->mbr_partition1->part_fit,"");
                    strcpy(mbrLeido->mbr_partition1->part_name,"");
                    strcpy(mbrLeido->mbr_partition1->part_type,"x");

                    fseek(barchivo1,0,SEEK_SET);
                    fwrite(mbrLeido,sizeof(mbr)+ sizeof( partition),1,barchivo1);
                    fseek(barchivo1,0,SEEK_SET);
                    fread(mbrLeido1,sizeof(mbr),1,barchivo1);
                    fclose(barchivo1);
                }
            }else if(strcmp(compro->mbr_partition2->part_name,letraName) == 0){
                if(strcmp(letraDelete,"full") == 0){
                    mbr *mbrLeido = (mbr *)malloc(sizeof(mbr));
                    mbr *mbrLeido1 = (mbr *)malloc(sizeof(mbr));
                    mbrLeido1->mbr_partition2 = (partition *)malloc(sizeof(partition));
                    FILE *barchivo1 = fopen(direccion,"r+b");
                    fseek(barchivo1,0,SEEK_SET);
                    fread(mbrLeido,sizeof(mbr),1,barchivo1);
                    mbrLeido->mbr_partition2 = (partition *)malloc(sizeof(partition));
                    mbrLeido->mbr_partition2->part_size = 0;
                    mbrLeido->mbr_partition2->part_start = 0;
                    strcpy(mbrLeido->mbr_partition2->part_fit,"");
                    strcpy(mbrLeido->mbr_partition2->part_name,"");
                    strcpy(mbrLeido->mbr_partition2->part_type,"x");

                    fseek(barchivo1,0,SEEK_SET);
                    fwrite(mbrLeido,sizeof(mbr)+ sizeof( partition),1,barchivo1);
                    fseek(barchivo1,0,SEEK_SET);
                    fread(mbrLeido1,sizeof(mbr),1,barchivo1);
                    fclose(barchivo1);
                }else if(strcmp(letraDelete,"fast") == 0){
                    mbr *mbrLeido = (mbr *)malloc(sizeof(mbr));
                    mbr *mbrLeido1 = (mbr *)malloc(sizeof(mbr));
                    mbrLeido1->mbr_partition2 = (partition *)malloc(sizeof(partition));
                    FILE *barchivo1 = fopen(direccion,"r+b");
                    fseek(barchivo1,0,SEEK_SET);
                    fread(mbrLeido,sizeof(mbr),1,barchivo1);
                    mbrLeido->mbr_partition2 = (partition *)malloc(sizeof(partition));
                    mbrLeido->mbr_partition2->part_size = 0;
                    mbrLeido->mbr_partition2->part_start = 0;
                    strcpy(mbrLeido->mbr_partition2->part_fit,"");
                    strcpy(mbrLeido->mbr_partition2->part_name,"");
                    strcpy(mbrLeido->mbr_partition2->part_type,"x");

                    fseek(barchivo1,0,SEEK_SET);
                    fwrite(mbrLeido,sizeof(mbr)+ sizeof( partition),1,barchivo1);
                    fseek(barchivo1,0,SEEK_SET);
                    fread(mbrLeido1,sizeof(mbr),1,barchivo1);
                    fclose(barchivo1);
                }
            }else if(strcmp(compro->mbr_partition3->part_name,letraName) == 0){
                if(strcmp(letraDelete,"full") == 0){
                    mbr *mbrLeido = (mbr *)malloc(sizeof(mbr));
                    mbr *mbrLeido1 = (mbr *)malloc(sizeof(mbr));
                    mbrLeido1->mbr_partition3 = (partition *)malloc(sizeof(partition));
                    FILE *barchivo1 = fopen(direccion,"r+b");
                    fseek(barchivo1,0,SEEK_SET);
                    fread(mbrLeido,sizeof(mbr),1,barchivo1);
                    mbrLeido->mbr_partition3 = (partition *)malloc(sizeof(partition));
                    mbrLeido->mbr_partition3->part_size = 0;
                    mbrLeido->mbr_partition3->part_start = 0;
                    strcpy(mbrLeido->mbr_partition3->part_fit,"");
                    strcpy(mbrLeido->mbr_partition3->part_name,"");
                    strcpy(mbrLeido->mbr_partition3->part_type,"x");

                    fseek(barchivo1,0,SEEK_SET);
                    fwrite(mbrLeido,sizeof(mbr)+ sizeof( partition),1,barchivo1);
                    fseek(barchivo1,0,SEEK_SET);
                    fread(mbrLeido1,sizeof(mbr),1,barchivo1);
                    fclose(barchivo1);
                }else if(strcmp(letraDelete,"fast") == 0){
                    mbr *mbrLeido = (mbr *)malloc(sizeof(mbr));
                    mbr *mbrLeido1 = (mbr *)malloc(sizeof(mbr));
                    mbrLeido1->mbr_partition3 = (partition *)malloc(sizeof(partition));
                    FILE *barchivo1 = fopen(direccion,"r+b");
                    fseek(barchivo1,0,SEEK_SET);
                    fread(mbrLeido,sizeof(mbr),1,barchivo1);
                    mbrLeido->mbr_partition3 = (partition *)malloc(sizeof(partition));
                    mbrLeido->mbr_partition3->part_size = 0;
                    mbrLeido->mbr_partition3->part_start = 0;
                    strcpy(mbrLeido->mbr_partition3->part_fit,"");
                    strcpy(mbrLeido->mbr_partition3->part_name,"");
                    strcpy(mbrLeido->mbr_partition3->part_type,"x");

                    fseek(barchivo1,0,SEEK_SET);
                    fwrite(mbrLeido,sizeof(mbr)+ sizeof( partition),1,barchivo1);
                    fseek(barchivo1,0,SEEK_SET);
                    fread(mbrLeido1,sizeof(mbr),1,barchivo1);
                    fclose(barchivo1);
                }
            }else if(strcmp(compro->mbr_partition4->part_name,letraName) == 0){
                if(strcmp(letraDelete,"full") == 0){
                    mbr *mbrLeido = (mbr *)malloc(sizeof(mbr));
                    mbr *mbrLeido1 = (mbr *)malloc(sizeof(mbr));
                    mbrLeido1->mbr_partition4 = (partition *)malloc(sizeof(partition));
                    FILE *barchivo1 = fopen(direccion,"r+b");
                    fseek(barchivo1,0,SEEK_SET);
                    fread(mbrLeido,sizeof(mbr),1,barchivo1);
                    mbrLeido->mbr_partition4 = (partition *)malloc(sizeof(partition));
                    mbrLeido->mbr_partition4->part_size = 0;
                    mbrLeido->mbr_partition4->part_start = 0;
                    strcpy(mbrLeido->mbr_partition4->part_fit,"");
                    strcpy(mbrLeido->mbr_partition4->part_name,"");
                    strcpy(mbrLeido->mbr_partition4->part_type,"x");

                    fseek(barchivo1,0,SEEK_SET);
                    fwrite(mbrLeido,sizeof(mbr)+ sizeof( partition),1,barchivo1);
                    fseek(barchivo1,0,SEEK_SET);
                    fread(mbrLeido1,sizeof(mbr),1,barchivo1);
                    fclose(barchivo1);
                }else if(strcmp(letraDelete,"fast") == 0){
                    mbr *mbrLeido = (mbr *)malloc(sizeof(mbr));
                    mbr *mbrLeido1 = (mbr *)malloc(sizeof(mbr));
                    mbrLeido1->mbr_partition4 = (partition *)malloc(sizeof(partition));
                    FILE *barchivo1 = fopen(direccion,"r+b");
                    fseek(barchivo1,0,SEEK_SET);
                    fread(mbrLeido,sizeof(mbr),1,barchivo1);
                    mbrLeido->mbr_partition4 = (partition *)malloc(sizeof(partition));
                    mbrLeido->mbr_partition4->part_size = 0;
                    mbrLeido->mbr_partition4->part_start = 0;
                    strcpy(mbrLeido->mbr_partition4->part_fit,"");
                    strcpy(mbrLeido->mbr_partition4->part_name,"");
                    strcpy(mbrLeido->mbr_partition4->part_type,"x");

                    fseek(barchivo1,0,SEEK_SET);
                    fwrite(mbrLeido,sizeof(mbr)+ sizeof( partition),1,barchivo1);
                    fseek(barchivo1,0,SEEK_SET);
                    fread(mbrLeido1,sizeof(mbr),1,barchivo1);
                    fclose(barchivo1);
                }
            }

        }else{
            printf("\nERROR, NO EXISTE EL DISCO\n");
        }
    }

    if(accion == 2){
        if(tamaAdd < 0){
            /*SE QUITA*/
            if(strcmp(compro->mbr_partition1->part_name,letraName) == 0){
                mbr *mbrLeido = (mbr *)malloc(sizeof(mbr));
                mbr *mbrLeido1 = (mbr *)malloc(sizeof(mbr));
                mbrLeido1->mbr_partition1 = (partition *)malloc(sizeof(partition));
                FILE *barchivo1 = fopen(direccion,"r+b");
                fseek(barchivo1,0,SEEK_SET);
                fread(mbrLeido,sizeof(mbr),1,barchivo1);
                int ao = mbrLeido->mbr_partition1->part_size + tamaAdd;
                if(ao < 0){
                    printf("\nError, tamanio a quitar es mas grande\n");
                }else{
                    mbrLeido->mbr_partition1->part_size = ao;
                }
                fseek(barchivo1,0,SEEK_SET);
                fwrite(mbrLeido,sizeof(mbr)+ sizeof( partition),1,barchivo1);
                fseek(barchivo1,0,SEEK_SET);
                fread(mbrLeido1,sizeof(mbr),1,barchivo1);
                fclose(barchivo1);
            }else if(strcmp(compro->mbr_partition2->part_name,letraName) == 0){
                mbr *mbrLeido = (mbr *)malloc(sizeof(mbr));
                mbr *mbrLeido1 = (mbr *)malloc(sizeof(mbr));
                mbrLeido1->mbr_partition2 = (partition *)malloc(sizeof(partition));
                FILE *barchivo1 = fopen(direccion,"r+b");
                fseek(barchivo1,0,SEEK_SET);
                fread(mbrLeido,sizeof(mbr),1,barchivo1);
                int ao = mbrLeido->mbr_partition2->part_size + tamaAdd;
                if(ao < 0){
                    printf("\nError, tamanio a quitar es mas grande\n");
                }else{
                    mbrLeido->mbr_partition2->part_size = ao;
                }
                fseek(barchivo1,0,SEEK_SET);
                fwrite(mbrLeido,sizeof(mbr)+ sizeof( partition),1,barchivo1);
                fseek(barchivo1,0,SEEK_SET);
                fread(mbrLeido1,sizeof(mbr),1,barchivo1);
                fclose(barchivo1);
            }else if(strcmp(compro->mbr_partition3->part_name,letraName) == 0){
                mbr *mbrLeido = (mbr *)malloc(sizeof(mbr));
                mbr *mbrLeido1 = (mbr *)malloc(sizeof(mbr));
                mbrLeido1->mbr_partition3 = (partition *)malloc(sizeof(partition));
                FILE *barchivo1 = fopen(direccion,"r+b");
                fseek(barchivo1,0,SEEK_SET);
                fread(mbrLeido,sizeof(mbr),1,barchivo1);
                int ao = mbrLeido->mbr_partition3->part_size + tamaAdd;
                if(ao < 0){
                    printf("\nError, tamanio a quitar es mas grande\n");
                }else{
                    mbrLeido->mbr_partition3->part_size = ao;
                }
                fseek(barchivo1,0,SEEK_SET);
                fwrite(mbrLeido,sizeof(mbr)+ sizeof( partition),1,barchivo1);
                fseek(barchivo1,0,SEEK_SET);
                fread(mbrLeido1,sizeof(mbr),1,barchivo1);
                fclose(barchivo1);
            }else if(strcmp(compro->mbr_partition4->part_name,letraName) == 0){
                mbr *mbrLeido = (mbr *)malloc(sizeof(mbr));
                mbr *mbrLeido1 = (mbr *)malloc(sizeof(mbr));
                mbrLeido1->mbr_partition4 = (partition *)malloc(sizeof(partition));
                FILE *barchivo1 = fopen(direccion,"r+b");
                fseek(barchivo1,0,SEEK_SET);
                fread(mbrLeido,sizeof(mbr),1,barchivo1);
                int ao = mbrLeido->mbr_partition4->part_size + tamaAdd;
                if(ao < 0){
                    printf("\nError, tamanio a quitar es mas grande\n");
                }else{
                    mbrLeido->mbr_partition4->part_size = ao;
                }
                fseek(barchivo1,0,SEEK_SET);
                fwrite(mbrLeido,sizeof(mbr)+ sizeof( partition),1,barchivo1);
                fseek(barchivo1,0,SEEK_SET);
                fread(mbrLeido1,sizeof(mbr),1,barchivo1);
                fclose(barchivo1);
            }else{
                printf("\nError, particion no existe.\n");
                return -1;
            }
        }else if(tamaAdd > 0){
            /*SE AGREGA TAMANIO A LA PARTICION*/
            if(strcmp(compro->mbr_partition1->part_name,letraName) == 0){
                mbr *mbrLeido = (mbr *)malloc(sizeof(mbr));
                mbr *mbrLeido1 = (mbr *)malloc(sizeof(mbr));
                mbrLeido1->mbr_partition1 = (partition *)malloc(sizeof(partition));
                FILE *barchivo1 = fopen(direccion,"r+b");
                fseek(barchivo1,0,SEEK_SET);
                fread(mbrLeido,sizeof(mbr),1,barchivo1);
                int ao = mbrLeido->mbr_partition1->part_size + tamaAdd;
                if(ao > mbrLeido->mbr_tamanio){
                    printf("\nError, tamanio de particion no puede aceptar mas\n");
                }else{
                        if(strcmp(compro->mbr_partition2->part_type,"x") == 0){
                            if(strcmp(compro->mbr_partition3->part_type,"x") == 0){
                                if(strcmp(compro->mbr_partition4->part_type,"x") == 0){
                                    mbrLeido->mbr_partition1->part_size = ao;
                                }else{
                                    if(ao <= (compro->mbr_partition4->part_start - compro->mbr_partition1->part_size - sizeof(mbr))){
                                        mbrLeido->mbr_partition1->part_size = ao;
                                    }
                                }
                            }else{
                                if(ao <= (compro->mbr_partition3->part_start - compro->mbr_partition1->part_size - sizeof(mbr))){
                                    mbrLeido->mbr_partition1->part_size = ao;
                                }
                            }
                        }else{
                            if(ao <= (compro->mbr_partition2->part_start - compro->mbr_partition1->part_size - sizeof(mbr))){
                                mbrLeido->mbr_partition1->part_size = ao;
                            }
                        }
                }
                fseek(barchivo1,0,SEEK_SET);
                fwrite(mbrLeido,sizeof(mbr)+ sizeof( partition),1,barchivo1);
                fseek(barchivo1,0,SEEK_SET);
                fread(mbrLeido1,sizeof(mbr),1,barchivo1);
                fclose(barchivo1);
            }else if(strcmp(compro->mbr_partition2->part_name,letraName) == 0){
                mbr *mbrLeido = (mbr *)malloc(sizeof(mbr));
                mbr *mbrLeido1 = (mbr *)malloc(sizeof(mbr));
                mbrLeido1->mbr_partition1 = (partition *)malloc(sizeof(partition));
                FILE *barchivo1 = fopen(direccion,"r+b");
                fseek(barchivo1,0,SEEK_SET);
                fread(mbrLeido,sizeof(mbr),1,barchivo1);
                int ao = mbrLeido->mbr_partition2->part_size + tamaAdd;
                
                if(ao > mbrLeido->mbr_tamanio){
                    printf("\nError, tamanio de particion no puede aceptar mas\n");
                }else{

                    if(strcmp(compro->mbr_partition1->part_type,"x") == 0){
                        int aa = compro->mbr_partition2->part_start;
                        if(aa > 0){
                            if(aa >= tamaAdd){
                                mbrLeido->mbr_partition2->part_size + tamaAdd;
                                mbrLeido->mbr_partition2->part_start - tamaAdd;
                            }else{
                                printf("\nNo hay espacio\n");
                            }
                        }else{
                            printf("\nNo hay espacio\n");
                        }
                    }else{
                        if(strcmp(compro->mbr_partition3->part_type,"x") == 0){
                            if(strcmp(compro->mbr_partition4->part_type,"x") == 0){
                                mbrLeido->mbr_partition2->part_size = ao;
                            }else{
                                if(ao <= (compro->mbr_partition4->part_start - compro->mbr_partition2->part_size - compro->mbr_partition2->part_start)){
                                    mbrLeido->mbr_partition2->part_size = ao;
                                }
                            }
                        }else{
                            if(ao <= (compro->mbr_partition3->part_start - compro->mbr_partition2->part_size - compro->mbr_partition2->part_start )){
                                mbrLeido->mbr_partition2->part_size = ao;
                            }
                        }
                    }

                }
                fseek(barchivo1,0,SEEK_SET);
                fwrite(mbrLeido,sizeof(mbr)+ sizeof( partition),1,barchivo1);
                fseek(barchivo1,0,SEEK_SET);
                fread(mbrLeido1,sizeof(mbr),1,barchivo1);
                fclose(barchivo1);
            }else if(strcmp(compro->mbr_partition3->part_name,letraName) == 0){
                mbr *mbrLeido = (mbr *)malloc(sizeof(mbr));
                mbr *mbrLeido1 = (mbr *)malloc(sizeof(mbr));
                mbrLeido1->mbr_partition1 = (partition *)malloc(sizeof(partition));
                FILE *barchivo1 = fopen(direccion,"r+b");
                fseek(barchivo1,0,SEEK_SET);
                fread(mbrLeido,sizeof(mbr),1,barchivo1);
                int ao = mbrLeido->mbr_partition3->part_size + tamaAdd;
                if(ao > mbrLeido->mbr_tamanio){
                    printf("\nError, tamanio de particion no puede aceptar mas\n");
                }else{

                }
                fseek(barchivo1,0,SEEK_SET);
                fwrite(mbrLeido,sizeof(mbr)+ sizeof( partition),1,barchivo1);
                fseek(barchivo1,0,SEEK_SET);
                fread(mbrLeido1,sizeof(mbr),1,barchivo1);
                fclose(barchivo1);
            }else if(strcmp(compro->mbr_partition4->part_name,letraName) == 0){
                mbr *mbrLeido = (mbr *)malloc(sizeof(mbr));
                mbr *mbrLeido1 = (mbr *)malloc(sizeof(mbr));
                mbrLeido1->mbr_partition1 = (partition *)malloc(sizeof(partition));
                FILE *barchivo1 = fopen(direccion,"r+b");
                fseek(barchivo1,0,SEEK_SET);
                fread(mbrLeido,sizeof(mbr),1,barchivo1);
                int ao = mbrLeido->mbr_partition3->part_size + tamaAdd;
                if(ao > mbrLeido->mbr_tamanio){
                    printf("\nError, tamanio de particion no puede aceptar mas\n");
                }else{
                    int aa = compro->mbr_partition3->part_size + compro->mbr_partition2->part_size + compro->mbr_partition1->part_size;
                    int ts = compro->mbr_tamanio - sizeof(mbr) - aa;
                    if(ts > tamaAdd){
                        if((compro->mbr_partition3->part_start + compro->mbr_partition3->part_size) == compro->mbr_partition4->part_start){
                            printf("\Error, No hay espacio");
                        }else{
                            mbrLeido->mbr_partition4->part_start - tamaAdd;
                            mbrLeido->mbr_partition4->part_size + tamaAdd;
                        }
                    }
                }
                fseek(barchivo1,0,SEEK_SET);
                fwrite(mbrLeido,sizeof(mbr)+ sizeof( partition),1,barchivo1);
                fseek(barchivo1,0,SEEK_SET);
                fread(mbrLeido1,sizeof(mbr),1,barchivo1);
                fclose(barchivo1);

            }else{
                printf("\nError, particion no existe.\n");
                return -1;
            }
        }
    }
}

int metodoMount(char *token)
{
    char path[10] = "-path";
    char name[10] = "-name";
    char comillas[3] = "\"";

    char guadoPath1[100];
    char guardoNombre[100];
    int control;

    while(token!=NULL)
    {
        token = strtok(NULL, " =");
        if(token==NULL)
            break;

        char *estaPath;
        char *estaName;

        estaPath = strstr(token,path);
        estaName = strstr(token,name);

        if(estaPath)
            control = 1;
        else if(estaName)
            control = 2;
        else
            control = 0;

        switch (control){
        case 1:
            token = strtok(NULL, " =");
            if(strstr(token,comillas)){
                strcpy(guadoPath1,token+1);
                token = strtok(NULL, "\"");
                strcat(guadoPath1," ");
                strcat(guadoPath1,token);
            }else{
                strcpy(guadoPath1,token);
            }
            break;
        case 2:
            token = strtok(NULL, " =");
            strcpy(guardoNombre,token);
            break;
        default:
            printf("\nError, comando no valido.\n");
            break;
        }
    }

    FILE *archivo = fopen(guadoPath1,"r+b");
    mbr *compro = (mbr *)malloc(sizeof(mbr));
    fseek(archivo,0,SEEK_SET);
    fread(compro,sizeof(mbr),1,archivo);

    if(archivo!=NULL){
        fclose(archivo);

        if(strcmp(compro->mbr_partition1->part_name,guardoNombre) == 0){
            agregarATabla(tablaDeParticiones,guadoPath1,guardoNombre);
        }else if(strcmp(compro->mbr_partition2->part_name,guardoNombre) == 0){
            agregarATabla(tablaDeParticiones,guadoPath1,guardoNombre);
        }else if(strcmp(compro->mbr_partition3->part_name,guardoNombre) == 0){
            agregarATabla(tablaDeParticiones,guadoPath1,guardoNombre);
        }else if(strcmp(compro->mbr_partition4->part_name,guardoNombre) == 0){
            agregarATabla(tablaDeParticiones,guadoPath1,guardoNombre);
        }else{
            printf("\nError, no existe esa particion en el disco.\n");
        }

    }else{
        printf("\nError, disco no existe");
    }


}

int metodoUnmount(char *token)
{
    char id[10] = "-id";

    char guardoId[100];
    int control;

    while(token!=NULL)
    {
        token = strtok(NULL, " =");
        if(token==NULL)
            break;

        char *estaId;

        estaId = strstr(token,id);

        if(estaId)
            control = 1;
        else
            control = 0;

        switch (control){
        case 1:
            token = strtok(NULL, "=");
            strcpy(guardoId,token);
            break;
        default:
            printf("\nError, comando no valido.\n");
            return -1;
            break;
        }
    }

    registro *aux = (registro *)malloc(sizeof(registro));
    registro *aux2 = (registro *)malloc(sizeof(registro));
    aux = tablaDeParticiones->primero;

    while(aux != NULL){

        if(strcmp(aux->id,guardoId) == 0){
            if(aux == tablaDeParticiones->primero && aux == tablaDeParticiones->ultimo){
                tablaDeParticiones->primero = NULL;
                tablaDeParticiones->ultimo = NULL;
                return 1;
            }else if(aux == tablaDeParticiones->ultimo){
                aux2 = aux->anterior;
                aux2->siguiente = NULL;
                tablaDeParticiones->ultimo = aux2;
                return 1;
            }else if(aux == tablaDeParticiones->primero && aux->siguiente != NULL){
                aux = aux->siguiente;
                aux->anterior = NULL;
                tablaDeParticiones->primero = aux;
                return 1;
            }else{
                registro *aux3 = (registro *)malloc(sizeof(registro));
                aux3 = aux->anterior;
                aux3->siguiente = aux->siguiente;
                registro *aux4 = (registro *)malloc(sizeof(registro));
                aux4 = aux->siguiente;
                aux4->anterior = aux->anterior;
                return 1;
            }
        }

        if(strcmp(aux->id,guardoId) != 0 && aux == tablaDeParticiones->ultimo){
            printf("\nError, no existe ID en tabla de particiones\n");
        }

        aux = aux->siguiente;
    }

}

int metodoRep(char *token)
{
    char path[10] = "-path";
    char name[10] = "-name";
    char id[10] = "-id";
    char comillas[3] = "\"";

    char guadoPath1[100];
    char guardoNombre[100];
    char guardoId[100];
    int control;

    while(token!=NULL)
    {
        printf( " %s\n", token );

        token = strtok(NULL, " =");
        if(token==NULL)
            break;

        char *estaPath;
        char *estaName;
        char *estaId;

        estaPath = strstr(token,path);
        estaName = strstr(token,name);
        estaId = strstr(token,id);

        if(estaPath)
            control = 1;
        else if(estaName)
            control = 2;
        else if(estaId)
            control = 3;
        else
            control = 0;

        switch (control){
        case 1:
            token = strtok(NULL, " =");

            if(strstr(token,comillas)){
                strcpy(guadoPath1,token+1);
                token = strtok(NULL, "\"");
                strcat(guadoPath1," ");
                strcat(guadoPath1,token);
                printf("\nCAAAAAAAAAADENA   : %s\n",guadoPath1);
            }else{
                strcpy(guadoPath1,token);
            }
            break;
        case 2:
            token = strtok(NULL, " =");
            strcpy(guardoNombre,token);
            break;
        case 3:
            token = strtok(NULL, " =");
            strcpy(guardoId,token);
            break;
        default:
            printf("\nError, comando no valido.\n");
            break;
        }
    }
}


int agregarATabla(tabla *t, char path [100],char nombre[100]){

    registro *aux = (registro *)malloc(sizeof(registro));
    strcpy(aux->path,path);
    strcpy(aux->name,nombre);
    aux->siguiente = NULL;
    aux->anterior = NULL;


    if(t->primero == NULL){

        strcpy(aux->id,"vda1");

        t->primero = aux;
        t->ultimo = aux;

    }else{

        int aa = 0;
        int bb = 0;
        int cc = 0;
        int dd = 0;
        int ee = 0;
        int ff = 0;
        int gg = 0;
        int hh = 0;
        int ii = 0;
        int jj = 0;
        int kk = 0;
        int ll = 0;
        int mm = 0;
        int nn = 0;
        int oo = 0;
        int pp = 0;

        int ingresar;

        registro *verificarSiExiste = (registro *)malloc(sizeof(registro));
        verificarSiExiste = t->primero;
        int siExiste;

        char vd1[5] = "";
        char vd2[5] = "";
        char vd3[5] = "";
        char vd4[5] = "";

        while(verificarSiExiste!=NULL){


            if(strstr(verificarSiExiste->name,nombre) && strstr(verificarSiExiste->path,path)){
                printf("\nLa particion ya esta montada\n");
                return -1;
            }


            if(strstr(verificarSiExiste->id,"vda")){
                aa = 1;
            }else if(strstr(verificarSiExiste->id,"vdb")){
                bb = 1;
            }else if(strstr(verificarSiExiste->id,"vdc")){
                cc = 1;
            }else if(strstr(verificarSiExiste->id,"vdd")){
                dd = 1;
            }else if(strstr(verificarSiExiste->id,"vde")){
                ee = 1;
            }else if(strstr(verificarSiExiste->id,"vdf")){
                ff = 1;
            }else if(strstr(verificarSiExiste->id,"vdg")){
                gg = 1;
            }else if(strstr(verificarSiExiste->id,"vdh")){
                hh = 1;
            }else if(strstr(verificarSiExiste->id,"vdi")){
                ii = 1;
            }else if(strstr(verificarSiExiste->id,"vdj")){
                jj = 1;
            }else if(strstr(verificarSiExiste->id,"vdk")){
                kk = 1;
            }else if(strstr(verificarSiExiste->id,"vdl")){
                ll = 1;
            }else if(strstr(verificarSiExiste->id,"vdm")){
                mm = 1;
            }


            if(strstr(verificarSiExiste->path,path)){

                if(strstr(verificarSiExiste->id,"vda1")){
                    strcpy(vd1,verificarSiExiste->id);
                }else if(strstr(verificarSiExiste->id,"vda2")){
                    strcpy(vd2,verificarSiExiste->id);
                }else if(strstr(verificarSiExiste->id,"vda3")){
                    strcpy(vd3,verificarSiExiste->id);
                }else if(strstr(verificarSiExiste->id,"vda4")){
                    strcpy(vd4,verificarSiExiste->id);
                }else if(strstr(verificarSiExiste->id,"vdb1")){
                    strcpy(vd1,verificarSiExiste->id);
                }else if(strstr(verificarSiExiste->id,"vdb2")){
                    strcpy(vd2,verificarSiExiste->id);
                }else if(strstr(verificarSiExiste->id,"vdb3")){
                    strcpy(vd3,verificarSiExiste->id);
                }else if(strstr(verificarSiExiste->id,"vdb4")){
                    strcpy(vd4,verificarSiExiste->id);
                }else if(strstr(verificarSiExiste->id,"vdb1")){
                    strcpy(vd1,verificarSiExiste->id);
                }else if(strstr(verificarSiExiste->id,"vdb2")){
                    strcpy(vd2,verificarSiExiste->id);
                }else if(strstr(verificarSiExiste->id,"vdb3")){
                    strcpy(vd3,verificarSiExiste->id);
                }else if(strstr(verificarSiExiste->id,"vdb4")){
                    strcpy(vd4,verificarSiExiste->id);
                }else if(strstr(verificarSiExiste->id,"vdc1")){
                    strcpy(vd1,verificarSiExiste->id);
                }else if(strstr(verificarSiExiste->id,"vdc2")){
                    strcpy(vd2,verificarSiExiste->id);
                }else if(strstr(verificarSiExiste->id,"vdc3")){
                    strcpy(vd3,verificarSiExiste->id);
                }else if(strstr(verificarSiExiste->id,"vdc4")){
                    strcpy(vd4,verificarSiExiste->id);
                }else if(strstr(verificarSiExiste->id,"vdd1")){
                    strcpy(vd1,verificarSiExiste->id);
                }else if(strstr(verificarSiExiste->id,"vdd2")){
                    strcpy(vd2,verificarSiExiste->id);
                }else if(strstr(verificarSiExiste->id,"vdd3")){
                    strcpy(vd3,verificarSiExiste->id);
                }else if(strstr(verificarSiExiste->id,"vdd4")){
                    strcpy(vd4,verificarSiExiste->id);
                }else if(strstr(verificarSiExiste->id,"vde1")){
                    strcpy(vd1,verificarSiExiste->id);
                }else if(strstr(verificarSiExiste->id,"vde2")){
                    strcpy(vd2,verificarSiExiste->id);
                }else if(strstr(verificarSiExiste->id,"vde3")){
                    strcpy(vd3,verificarSiExiste->id);
                }else if(strstr(verificarSiExiste->id,"vde4")){
                    strcpy(vd4,verificarSiExiste->id);
                }else if(strstr(verificarSiExiste->id,"vdf1")){
                    strcpy(vd1,verificarSiExiste->id);
                }else if(strstr(verificarSiExiste->id,"vdf2")){
                    strcpy(vd2,verificarSiExiste->id);
                }else if(strstr(verificarSiExiste->id,"vdf3")){
                    strcpy(vd3,verificarSiExiste->id);
                }else if(strstr(verificarSiExiste->id,"vdf4")){
                    strcpy(vd4,verificarSiExiste->id);
                }else if(strstr(verificarSiExiste->id,"vdg1")){
                    strcpy(vd1,verificarSiExiste->id);
                }else if(strstr(verificarSiExiste->id,"vdg2")){
                    strcpy(vd2,verificarSiExiste->id);
                }else if(strstr(verificarSiExiste->id,"vdg3")){
                    strcpy(vd3,verificarSiExiste->id);
                }else if(strstr(verificarSiExiste->id,"vdg4")){
                    strcpy(vd4,verificarSiExiste->id);
                }else if(strstr(verificarSiExiste->id,"vdh1")){
                    strcpy(vd1,verificarSiExiste->id);
                }else if(strstr(verificarSiExiste->id,"vdh2")){
                    strcpy(vd2,verificarSiExiste->id);
                }else if(strstr(verificarSiExiste->id,"vdh3")){
                    strcpy(vd3,verificarSiExiste->id);
                }else if(strstr(verificarSiExiste->id,"vdh4")){
                    strcpy(vd4,verificarSiExiste->id);
                }
                siExiste = 1;
                //break;
            }

            verificarSiExiste = verificarSiExiste->siguiente;
        }

        if(siExiste == 1){

            if(strstr(vd1,"vdz1") || strstr(vd2,"vdz2") || strstr(vd3,"vdz3") || strstr(vd4,"vdz4")){

            }else if(strstr(vd1,"vda1") || strstr(vd2,"vda2") || strstr(vd3,"vda3") || strstr(vd4,"vda4")){
                if(strcmp(vd1,"") == 0){
                    strcpy(aux->id,"vda1");
                    ingresar = 1;
                }else if(strcmp(vd2,"") == 0){
                    strcpy(aux->id,"vda2");
                    ingresar = 1;
                }else if(strcmp(vd3,"") == 0){
                    strcpy(aux->id,"vda3");
                    ingresar = 1;
                }else if(strcmp(vd4,"") == 0){
                    strcpy(aux->id,"vda4");
                    ingresar = 1;
                }else{
                    printf("\nNo se pudo agregar\n");
                }
            }else if(strstr(vd1,"vdb1") || strstr(vd2,"vdb2") || strstr(vd3,"vdb3") || strstr(vd4,"vdb4")){
                if(strcmp(vd1,"") == 0){
                    strcpy(aux->id,"vdb1");
                    ingresar = 1;
                }else if(strcmp(vd2,"") == 0){
                    strcpy(aux->id,"vdb2");
                    ingresar = 1;
                }else if(strcmp(vd3,"") == 0){
                    strcpy(aux->id,"vdb3");
                    ingresar = 1;
                }else if(strcmp(vd4,"") == 0){
                    strcpy(aux->id,"vdb4");
                    ingresar = 1;
                }else{
                    printf("\nNo se pudo agregar\n");
                }
            }else if(strstr(vd1,"vdc1") || strstr(vd2,"vdc2") || strstr(vd3,"vdc3") || strstr(vd4,"vdc4")){
                if(strcmp(vd1,"") == 0){
                    strcpy(aux->id,"vdc1");
                    ingresar = 1;
                }else if(strcmp(vd2,"") == 0){
                    strcpy(aux->id,"vdc2");
                    ingresar = 1;
                }else if(strcmp(vd3,"") == 0){
                    strcpy(aux->id,"vdc3");
                    ingresar = 1;
                }else if(strcmp(vd4,"") == 0){
                    strcpy(aux->id,"vdc4");
                    ingresar = 1;
                }else{
                    printf("\nNo se pudo agregar\n");
                }
            }else if(strstr(vd1,"vdd1") || strstr(vd2,"vdd2") || strstr(vd3,"vdd3") || strstr(vd4,"vdd4")){
                if(strcmp(vd1,"") == 0){
                    strcpy(aux->id,"vdd1");
                    ingresar = 1;
                }else if(strcmp(vd2,"") == 0){
                    strcpy(aux->id,"vdd2");
                    ingresar = 1;
                }else if(strcmp(vd3,"") == 0){
                    strcpy(aux->id,"vdd3");
                    ingresar = 1;
                }else if(strcmp(vd4,"") == 0){
                    strcpy(aux->id,"vdd4");
                    ingresar = 1;
                }else{
                    printf("\nNo se pudo agregar\n");
                }
            }else if(strstr(vd1,"vde1") || strstr(vd2,"vde2") || strstr(vd3,"vde3") || strstr(vd4,"vde4")){
                if(strcmp(vd1,"") == 0){
                    strcpy(aux->id,"vde1");
                    ingresar = 1;
                }else if(strcmp(vd2,"") == 0){
                    strcpy(aux->id,"vde2");
                    ingresar = 1;
                }else if(strcmp(vd3,"") == 0){
                    strcpy(aux->id,"vde3");
                    ingresar = 1;
                }else if(strcmp(vd4,"") == 0){
                    strcpy(aux->id,"vde4");
                    ingresar = 1;
                }else{
                    printf("\nNo se pudo agregar\n");
                }
            }else if(strstr(vd1,"vdf1") || strstr(vd2,"vdf2") || strstr(vd3,"vdf3") || strstr(vd4,"vdf4")){
                if(strcmp(vd1,"") == 0){
                    strcpy(aux->id,"vdf1");
                    ingresar = 1;
                }else if(strcmp(vd2,"") == 0){
                    strcpy(aux->id,"vdf2");
                    ingresar = 1;
                }else if(strcmp(vd3,"") == 0){
                    strcpy(aux->id,"vdf3");
                    ingresar = 1;
                }else if(strcmp(vd4,"") == 0){
                    strcpy(aux->id,"vdf4");
                    ingresar = 1;
                }else{
                    printf("\nNo se pudo agregar\n");
                }
            }else if(strstr(vd1,"vdg1") || strstr(vd2,"vdg2") || strstr(vd3,"vdg3") || strstr(vd4,"vdg4")){
                if(strcmp(vd1,"") == 0){
                    strcpy(aux->id,"vdg1");
                    ingresar = 1;
                }else if(strcmp(vd2,"") == 0){
                    strcpy(aux->id,"vdg2");
                    ingresar = 1;
                }else if(strcmp(vd3,"") == 0){
                    strcpy(aux->id,"vdg3");
                    ingresar = 1;
                }else if(strcmp(vd4,"") == 0){
                    strcpy(aux->id,"vdg4");
                    ingresar = 1;
                }else{
                    printf("\nNo se pudo agregar\n");
                }
            }else if(strstr(vd1,"vdh1") || strstr(vd2,"vdh2") || strstr(vd3,"vdh3") || strstr(vd4,"vdh4")){
                if(strcmp(vd1,"") == 0){
                    strcpy(aux->id,"vdh1");
                    ingresar = 1;
                }else if(strcmp(vd2,"") == 0){
                    strcpy(aux->id,"vdh2");
                    ingresar = 1;
                }else if(strcmp(vd3,"") == 0){
                    strcpy(aux->id,"vdh3");
                    ingresar = 1;
                }else if(strcmp(vd4,"") == 0){
                    strcpy(aux->id,"vdh4");
                    ingresar = 1;
                }else{
                    printf("\nNo se pudo agregar\n");
                }
            }else if(strstr(vd1,"vdi1") || strstr(vd2,"vdi2") || strstr(vd3,"vdi3") || strstr(vd4,"vdi4")){
                if(strcmp(vd1,"") == 0){
                    strcpy(aux->id,"vdi1");
                    ingresar = 1;
                }else if(strcmp(vd2,"") == 0){
                    strcpy(aux->id,"vdi2");
                    ingresar = 1;
                }else if(strcmp(vd3,"") == 0){
                    strcpy(aux->id,"vdi3");
                    ingresar = 1;
                }else if(strcmp(vd4,"") == 0){
                    strcpy(aux->id,"vdi4");
                    ingresar = 1;
                }else{
                    printf("\nNo se pudo agregar\n");
                }
            }else if(strstr(vd1,"vdj1") || strstr(vd2,"vdj2") || strstr(vd3,"vdj3") || strstr(vd4,"vdj4")){
                if(strcmp(vd1,"") == 0){
                    strcpy(aux->id,"vdj1");
                    ingresar = 1;
                }else if(strcmp(vd2,"") == 0){
                    strcpy(aux->id,"vdj2");
                    ingresar = 1;
                }else if(strcmp(vd3,"") == 0){
                    strcpy(aux->id,"vdj3");
                    ingresar = 1;
                }else if(strcmp(vd4,"") == 0){
                    strcpy(aux->id,"vdj4");
                    ingresar = 1;
                }else{
                    printf("\nNo se pudo agregar\n");
                }
            }else if(strstr(vd1,"vdk1") || strstr(vd2,"vdk2") || strstr(vd3,"vdk3") || strstr(vd4,"vdk4")){
                if(strcmp(vd1,"") == 0){
                    strcpy(aux->id,"vdk1");
                    ingresar = 1;
                }else if(strcmp(vd2,"") == 0){
                    strcpy(aux->id,"vdk2");
                    ingresar = 1;
                }else if(strcmp(vd3,"") == 0){
                    strcpy(aux->id,"vdk3");
                    ingresar = 1;
                }else if(strcmp(vd4,"") == 0){
                    strcpy(aux->id,"vdk4");
                    ingresar = 1;
                }else{
                    printf("\nNo se pudo agregar\n");
                }
            }else if(strstr(vd1,"vdl1") || strstr(vd2,"vdl2") || strstr(vd3,"vdl3") || strstr(vd4,"vdl4")){
                if(strcmp(vd1,"") == 0){
                    strcpy(aux->id,"vdl1");
                    ingresar = 1;
                }else if(strcmp(vd2,"") == 0){
                    strcpy(aux->id,"vdl2");
                    ingresar = 1;
                }else if(strcmp(vd3,"") == 0){
                    strcpy(aux->id,"vdl3");
                    ingresar = 1;
                }else if(strcmp(vd4,"") == 0){
                    strcpy(aux->id,"vdl4");
                    ingresar = 1;
                }else{
                    printf("\nNo se pudo agregar\n");
                }
            }else if(strstr(vd1,"vdm1") || strstr(vd2,"vdm2") || strstr(vd3,"vdm3") || strstr(vd4,"vdm4")){
                if(strcmp(vd1,"") == 0){
                    strcpy(aux->id,"vdm1");
                    ingresar = 1;
                }else if(strcmp(vd2,"") == 0){
                    strcpy(aux->id,"vdm2");
                    ingresar = 1;
                }else if(strcmp(vd3,"") == 0){
                    strcpy(aux->id,"vdm3");
                    ingresar = 1;
                }else if(strcmp(vd4,"") == 0){
                    strcpy(aux->id,"vdm4");
                    ingresar = 1;
                }else{
                    printf("\nNo se pudo agregar\n");
                }
            }else if(strstr(vd1,"vdn1") || strstr(vd2,"vdn2") || strstr(vd3,"vdn3") || strstr(vd4,"vdn4")){
                if(strcmp(vd1,"") == 0){
                    strcpy(aux->id,"vdn1");
                    ingresar = 1;
                }else if(strcmp(vd2,"") == 0){
                    strcpy(aux->id,"vdn2");
                    ingresar = 1;
                }else if(strcmp(vd3,"") == 0){
                    strcpy(aux->id,"vdn3");
                    ingresar = 1;
                }else if(strcmp(vd4,"") == 0){
                    strcpy(aux->id,"vdn4");
                    ingresar = 1;
                }else{
                    printf("\nNo se pudo agregar\n");
                }
            }else if(strstr(vd1,"vdo1") || strstr(vd2,"vdo2") || strstr(vd3,"vdo3") || strstr(vd4,"vdo4")){
                if(strcmp(vd1,"") == 0){
                    strcpy(aux->id,"vdo1");
                    ingresar = 1;
                }else if(strcmp(vd2,"") == 0){
                    strcpy(aux->id,"vdo2");
                    ingresar = 1;
                }else if(strcmp(vd3,"") == 0){
                    strcpy(aux->id,"vdo3");
                    ingresar = 1;
                }else if(strcmp(vd4,"") == 0){
                    strcpy(aux->id,"vdo4");
                    ingresar = 1;
                }else{
                    printf("\nNo se pudo agregar\n");
                }
            }

            if(ingresar == 1){
                t->primero->anterior = aux;
                aux->siguiente = t->primero;
                t->primero = aux;
            }


        }else{

            if(aa == 0){
                strcpy(aux->id,"vda1");
                ingresar = 1;
            }else if(bb == 0){
                strcpy(aux->id,"vdb1");
                ingresar = 1;
            }else if(cc == 0){
                strcpy(aux->id,"vdc1");
                ingresar = 1;
            }else if(dd == 0){
                strcpy(aux->id,"vdd1");
                ingresar = 1;
            }else if(ee == 0){
                strcpy(aux->id,"vde1");
                ingresar = 1;
            }else if(ff == 0){
                strcpy(aux->id,"vdf1");
                ingresar = 1;
            }else if(gg == 0){
                strcpy(aux->id,"vdg1");
                ingresar = 1;
            }else if(hh == 0){
                strcpy(aux->id,"vdh1");
                ingresar = 1;
            }else if(ii == 0){
                strcpy(aux->id,"vdi1");
                ingresar = 1;
            }else if(jj == 0){
                strcpy(aux->id,"vdj1");
                ingresar = 1;
            }else if(kk == 0){
                strcpy(aux->id,"vdk1");
                ingresar = 1;
            }else if(ll == 0){
                strcpy(aux->id,"vdl1");
                ingresar = 1;
            }else if(mm == 0){
                strcpy(aux->id,"vdm1");
                ingresar = 1;
            }else if(nn == 0){
                strcpy(aux->id,"vdn1");
                ingresar = 1;
            }else if(oo == 0){
                strcpy(aux->id,"vdo1");
                ingresar = 1;
            }else if(pp == 0){
                strcpy(aux->id,"vdp1");
                ingresar = 1;
            }

            if(ingresar == 1){
                t->primero->anterior = aux;
                aux->siguiente = t->primero;
                t->primero = aux;
            }

        }

    }

}
























